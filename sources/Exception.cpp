#include "main.h"

//エラーカウンタの初期化
int Exception::count = 0;

//コンストラクタ1
Exception::Exception(const std::string str) :errorString(str), errorCode(0)
{}

//コンストラクタ2
Exception::Exception(const std::string str, const int code) : errorString(str), errorCode(code)
{}

//デストラクタ
Exception::~Exception()
{
}

//アップデート
void Exception::update()
{
	if (this->count < 2 && this->errorCode == 0)
	{
		MessageBox(
			NULL,
			("Error:" + this->errorString).c_str(),
			TEXT("Error"), MB_OK);
		this->count++;
	}
}

//エラー内容の取得
const std::string Exception::get() const
{
	return this->errorString;
}