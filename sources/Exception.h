#pragma once
#include <string>
#include "DxLib.h"
class Exception
{
private:
	const std::string errorString;//エラーメッセージ
	const int errorCode;//エラーコード
public:
	static int count;//エラーの出力回数
	Exception(const std::string);
	Exception(const std::string, const int code);
	~Exception();

	void update();
	const std::string get() const;
};
