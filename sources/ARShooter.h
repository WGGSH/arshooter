#pragma once

class ARShooter
{
private:
	int preInitialize();//DxLib_Init以前に行うべき初期化処理
	int afterInitialize();//DxLib_Init以降に行うべき初期化処理

	void processLoop();//メインループ
public:
	ARShooter();
	~ARShooter();

	void initalize();
	void update();

};
