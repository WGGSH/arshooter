#include "main.h"
using namespace namespaceARShooter;

//コンストラクタ
ARShooter::ARShooter()
{
	//クラスのインスタンス生成

	this->initalize();
}

//デストラクタ
ARShooter::~ARShooter()
{
	if (DxLib::DxLib_End() == -1)throw Exception("DxLib_End");
}

//初期化処理
void ARShooter::initalize()
{
	try
	{
		if (this->preInitialize() == -1)throw Exception("preInitialize");

		if (DxLib::DxLib_Init() == -1)throw  Exception("DxLib_Init");

		if (this->afterInitialize() == -1)throw Exception("afterInitialize");
	}
	catch(Exception &error)
	{
		error.update();
	}
}

//メインアップデート
void ARShooter::update()
{
	this->processLoop();
}

//---------------------

//DxLib_Init()以前の初期化処理
int ARShooter::preInitialize()
{
	if (DxLib::ChangeWindowMode(TRUE) == -1)return -1;
	if (DxLib::SetGraphMode(1280, 720, 32) == -1)return -1;
	if (DxLib::SetWindowSizeChangeEnableFlag(TRUE) == -1)return -1;

	return 0;
}

//DxLib_Init()以降の初期化処理
int ARShooter::afterInitialize()
{

	if (DxLib::SetDrawScreen(DX_SCREEN_BACK) == -1)return -1;
	if (DxLib::SetUseZBuffer3D(TRUE) == -1)return -1;
	//if (DxLib::SetUse3DFlag(TRUE) == -1)return -1;
	if (DxLib::SetWriteZBuffer3D(TRUE) == -1)return -1;

	return 0;
}

//メインループ
//3大必須処理を実行
//ProcessMessage(プロセス管理
//ClearDrawScreen描画内容の削除)
//~~~メイン処理
//ScreenFlip(裏画面を表画面に複製)
void ARShooter::processLoop()
{
	while (1)
	{
		if (DxLib::ProcessMessage() == -1)throw Exception("ProcessMessage",NO_ASSERT);
		if (DxLib::ClearDrawScreen() == -1)throw Exception("ClearDrawScreen");

		//メイン処理を書く

		if (DxLib::ScreenFlip() == -1)throw("ClearDrawScreen");
	}
}