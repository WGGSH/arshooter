#include "main.h"


int WINAPI WinMain(HINSTANCE hInstance, HINSTANCE hPrevInstance, LPSTR lpCmdLine, int nCmdShow)
{
	ARShooter *arShooter=nullptr;
	try
	{
		arShooter = new ARShooter();
		arShooter->update();
	}
	catch (Exception &error)
	{
		error.update();
		delete arShooter;
		return 0;
	}
	delete arShooter;
	return 0;
}
